import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Toplayer from './toplayer'
import Middlelayer from './middlelayer'
import Menulayer from './menulayer'

import { store } from 'src/store'

const lightColor = 'rgba(255, 255, 255, 0.7)';

const styles = (theme) => ({
  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
});

function Header(props) {
  const { onDrawerToggle } = props;

  return (
    <React.Fragment>

      <Toplayer onDrawerToggle={onDrawerToggle} store={store} />

      <Middlelayer  store={store} />

      <Menulayer  store={store} />
    
    </React.Fragment>
  );
}

Header.propTypes = {
  // classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(Header);
