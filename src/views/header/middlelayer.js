import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import HelpIcon from '@material-ui/icons/Help';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { useLocation } from 'react-router-dom';

import { count } from 'src/store/actions'

const styles = (theme) => ({
})

const Middlelayer = (props) => {
  const { classes, store } = props;
  const location = useLocation()
  const [ccount, setcconst] = useState(0)

  useEffect(() => {
    console.log(store)
  }, [store])

  const teststate = () => {
    var temp = ccount
    temp++
    setcconst(temp)
    store.dispatch(count(ccount))
  }

  return (
    <React.Fragment>
      <AppBar
        component="div"
        className={classes.secondaryBar}
        color="primary"
        position="static"
        elevation={0}
      >
        <Toolbar>
          <Grid container alignItems="center" spacing={1}>
            <Grid item xs>
              <Typography color="inherit" variant="h5" component="h1">
                {/* Authentication */}
                {
                  location.pathname
                }
              </Typography>
            </Grid>
            <Grid item>
              <Button className={classes.button} variant="outlined" color="inherit" size="small" onClick={teststate}>
                Web setup
              </Button>
            </Grid>
            <Grid item>
              <Tooltip title="Help">
                <IconButton color="inherit">
                  <HelpIcon />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  )
}

Middlelayer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Middlelayer);