import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';

import UserMenu from 'src/components/userMenu'

const styles = (theme) => ({

})

const Toplayer = (props) => {
  const { classes, onDrawerToggle, store} = props;
  const [count, setcount] = useState(0)

  useEffect(() => {

    setInterval(() => {
      setcount(store.getState().count)
    }, 1);


  }, [store])

  return (
    <React.Fragment>
      {/* elevation={0} -> appbar ค้างอยู่ด้านบน */}
      <AppBar color="primary" position="sticky" elevation={0}>
        <Toolbar>
          {/* component 1 */}
          <Grid container spacing={1} alignItems="center">
            <Hidden smUp>
              <Grid item>
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  onClick={onDrawerToggle}
                  className={classes.menuButton}
                >
                  <MenuIcon />
                </IconButton>
              </Grid>
            </Hidden>
          <Grid item xs />

          {/* component 2 */}
          {/* <Grid item>
            <Link className={classes.link} href="#" variant="body2">
              Go to docs
            </Link>
          </Grid> */}

          <Grid item>
            {
              count
            }
          </Grid>

          
            {/* component 3 */}
            <Grid item>
              <Tooltip title="Alerts • No alerts">
                <IconButton color="inherit">
                  <NotificationsIcon />
                </IconButton>
              </Tooltip>
            </Grid>

            {/* component 4 */}
            <Grid item>
              {/* <IconButton color="inherit" className={classes.iconButtonAvatar}>
                <Avatar src="/static/images/avatar/1.jpg" alt="My Avatar" />
              </IconButton> */}
              <UserMenu store={store} />
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>

    </React.Fragment>
  )
}

Toplayer.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrawerToggle: PropTypes.func.isRequired,
};

export default withStyles(styles)(Toplayer);