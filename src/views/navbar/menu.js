
import PeopleIcon from '@material-ui/icons/People';
import DnsRoundedIcon from '@material-ui/icons/DnsRounded';
import PermMediaOutlinedIcon from '@material-ui/icons/PhotoSizeSelectActual';
import PublicIcon from '@material-ui/icons/Public';
import SettingsEthernetIcon from '@material-ui/icons/SettingsEthernet';
import SettingsInputComponentIcon from '@material-ui/icons/SettingsInputComponent';
import TimerIcon from '@material-ui/icons/Timer';
import SettingsIcon from '@material-ui/icons/Settings';
import PhonelinkSetupIcon from '@material-ui/icons/PhonelinkSetup';

const Menu = [
  {
    id: 'Develop',
    children: [
      { id: 'Authentication', icon: <PeopleIcon />, link: "/app/Authentication" },
      { id: 'Database', icon: <DnsRoundedIcon />, link: "/app/Database" },
      { id: 'Storage', icon: <PermMediaOutlinedIcon />, link: "/app/Storage" },
      { id: 'Hosting', icon: <PublicIcon />, link: "/app/Hosting" },
      { id: 'Functions', icon: <SettingsEthernetIcon />, link: "/app/Functions" },
      { id: 'ML Kit', icon: <SettingsInputComponentIcon />, link: "/app/ML" },
    ],
  },
  {
    id: 'Quality',
    children: [
      { id: 'Analytics', icon: <SettingsIcon />, link: "/"},
      { id: 'Performance', icon: <TimerIcon />, link: "/"},
      { id: 'Test Lab', icon: <PhonelinkSetupIcon />, link: "/"},
    ],
  },
  {
    id: 'Test',
    children: [
      { id: 'test1', icon: <SettingsIcon />, link: "/"},
      { id: 'test2', icon: <TimerIcon />, link: "/"},
      { id: 'test3', icon: <PhonelinkSetupIcon />, link: "/"},
    ],
  },
  
];

export default Menu