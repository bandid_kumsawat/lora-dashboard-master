import React from 'react'
import ListItem from '@material-ui/core/ListItem';
import { withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
// import image_logo from 'src/images/logo-test.png'

const styles = (theme) => ({
  item: {
    paddingTop: 1,
    paddingBottom: 1,
    color: 'rgba(255, 255, 255, 0.7)',
    '&:hover,&:focus': {
      backgroundColor: 'rgba(255, 255, 255, 0.08)',
    },
  },
  itemCategory: {
    backgroundColor: '#232f3e',
    boxShadow: '0 -1px 0 #404854 inset',
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  firebase: {
    fontSize: 24,
    color: theme.palette.common.white,
  },
});

const Logo = (props) => {

  const { classes } = props;

  return (
    <div>
      <ListItem className={clsx(classes.firebase, classes.item, classes.itemCategory)}>
        {/* <img src={image_logo} width={150} height={50}></img> */}
        App
      </ListItem>
    </div>
  )
}
export default withStyles(styles)(Logo);