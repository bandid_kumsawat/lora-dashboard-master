import React, {useState, useEffect} from 'react';

const Database = () => {
  
  const [data, setdata] = useState('')

  useEffect(() => {
    setdata(<div>DB</div>)
  }, [])

  return (
    <div>
      {
        data
      }
    </div>
  );
};

export default Database;
