import React, {useState, useEffect} from 'react';

const Functions = () => {
  
  const [data, setdata] = useState('')

  useEffect(() => {
    setdata(<div>Functions</div>)
  }, [])

  return (
    <div>
      {
        data
      }
    </div>
  );
};

export default Functions;
