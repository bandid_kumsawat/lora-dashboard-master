import React, {useState, useEffect} from 'react';

const Storage = () => {
  
  const [data, setdata] = useState('')

  useEffect(() => {
    setdata(<div>Storage</div>)
  }, [])

  return (
    <div>
      {
        data
      }
    </div>
  );
};

export default Storage;
