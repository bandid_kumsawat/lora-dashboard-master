import React, {useState, useEffect} from 'react';

const Ml = () => {
  
  const [data, setdata] = useState('')

  useEffect(() => {
    setdata(<div>ML</div>)
  }, [])

  return (
    <div>
      {
        data
      }
    </div>
  );
};

export default Ml;
