import React, {useState, useEffect} from 'react';

const Hosting = () => {
  
  const [data, setdata] = useState('')

  useEffect(() => {
    setdata(<div>Hosting</div>)
  }, [])

  return (
    <div>
      {
        data
      }
    </div>
  );
};

export default Hosting;
