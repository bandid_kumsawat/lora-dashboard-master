export const email = (payload) => {
  return {
    type : "email",
    payload
  }
}

export const type = (payload) => {
  return {
    type : "type",
    payload
  }
}

export const AccessToken = (payload) => {
  return {
    type : "AccessToken",
    payload
  }
}

export const count = (payload) => {
  return {
    type: 'count',
    payload
  }
}
