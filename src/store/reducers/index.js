import initialState from './state'

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case 'email':
      return Object.assign({}, state, {
        email: action.payload
      })
    case 'type':
      return Object.assign({}, state, {
        type: action.payload
      })
    case 'AccessToken':
      return Object.assign({}, state, {
        AccessToken: action.payload
      })
    case 'count':
      return Object.assign({}, state, {
        count: action.payload
      })
    default:
      return state
  }
}

export default reducers