import 'react-perfect-scrollbar/dist/css/styles.css';
import React from 'react';
import { useRoutes } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from 'src/components/GlobalStyles';
import routes from 'src/routes';
import './App.css'
import theme from 'src/theme'

const App = () => {

  const routing = useRoutes(routes);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {routing}
    </ThemeProvider>
  );
};

export default App;


// import './App.css';
// import Mainapp from './paperbase/Paperbase'

// function App() {
//   return (
//     <>
//       <Mainapp />
//     </>
//   );
// }

// export default App;
// // npm install @material-ui/core
// // npm install @material-ui/icons



