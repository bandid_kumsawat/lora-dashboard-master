import React from 'react';
import { Navigate } from 'react-router-dom';
import MainLayer from 'src/views/mainlayer'
import Authentication from 'src/views/dashbard/Authentication'
import Database from 'src/views/dashbard/Database'
import Functions from 'src/views/dashbard/Functions'
import Hosting from 'src/views/dashbard/Hosting'
import ML from 'src/views/dashbard/ML'
import Storage from 'src/views/dashbard/Storage'
import { store } from 'src/store'

const routes = [
  {
    path: 'app',
    element: <MainLayer />,
    children: [
      {path: "Authentication", element: <Authentication store={store} />},
      {path: "Database", element: <Database store={store} />},
      {path: "Functions", element: <Functions store={store} />},
      {path: "Hosting", element: <Hosting store={store} />},
      {path: "Storage", element: <Storage store={store} />},
      {path: "ML", element: <ML store={store} />},
      {path: "*", element: <Authentication store={store} />}
    ]
  },
  {
    path: '/',
    element: <MainLayer />,
    children: [
      { path: '/', element: <Navigate to="/app/Authentication" /> },
      { path: '*', element: <Navigate to="/" /> }
    ]
  }
];

export default routes;
